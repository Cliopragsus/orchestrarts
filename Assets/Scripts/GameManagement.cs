﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
	void Start ()
    {
        Cursor.visible = false;
        TextPopUpController.InitializeTextController();
        Cursor.lockState = CursorLockMode.Confined;
	}

    private void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
}
