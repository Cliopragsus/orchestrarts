﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryScript : MonoBehaviour
{
    private Rigidbody rBody;
    public float thrust;

	void Start ()
    {
        rBody = gameObject.GetComponent<Rigidbody>();
        rBody.AddForce(transform.forward * thrust);
	}
}
