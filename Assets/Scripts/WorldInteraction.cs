﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldInteraction : MonoBehaviour
{
    private UnityEngine.AI.NavMeshAgent tNMA; //target's Nav Mesh Agent
    private GameObject playerMember;

    public GameObject interactedObject;
    public GameObject lastLegal;
    public Vector3 interactionPoint;

    public GameObject levelle;
    public GameObject goldzunge;
    public GameObject byson;


    void Update()
    {
        Ray interactionRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit interactionInfo;
        Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity);
        if (interactionInfo.collider != null)
        {
            interactedObject = interactionInfo.collider.gameObject;
            interactionPoint = interactionInfo.point;
            {
                if (interactionInfo.collider.CompareTag("Player Member"))
                {
                    lastLegal = interactionInfo.collider.gameObject;
                }
            }
        }

        ///////////// Commanding by mouse.
        if (Input.GetMouseButtonDown(0))
        {
            GetInteraction();
        }

        if (Input.GetMouseButtonUp(0))
        {
            Command();
        }

        ///////////// Accessing abilities via q and e.
        if (Input.GetButtonDown("AbilityONE") && lastLegal.GetComponent<MemberVirtuals>().isActivating == false)
        {
            lastLegal.GetComponent<MemberVirtuals>().isActivating = true;
            lastLegal.GetComponent<MemberVirtuals>().Abilities();
        }

        if (Input.GetButtonDown("AbilityTWO") && lastLegal.GetComponent<MemberVirtuals>().isActivating == false)
        {
            lastLegal.GetComponent<MemberVirtuals>().isActivating = true;
            lastLegal.GetComponent<MemberVirtuals>().Abilities();
        }

        /////////////////////////////////////////
        if (Input.GetButtonDown("ONE"))
        {
            lastLegal = levelle;
        }
        if (Input.GetButtonDown("TWO"))
        {
            lastLegal = goldzunge;
        }
        if (Input.GetButtonDown("THREE"))
        {
            lastLegal = byson;
        }


    }



////////////////////////////////////     Selecting Member Individually










    void GetInteraction()  //on mouse down
    {
        if (interactedObject.tag == "Interactable Object")
        {
            Debug.Log("Hit an interactable.");
        }
        else if (interactedObject.tag == "Player Member")
        {
            playerMember = interactedObject.gameObject;
            tNMA = interactedObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
            Debug.Log("Hit a Player Member");
        }
    }

    void Command()        //on mouse up
    {
        Ray commandRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit commandInfo;
        if (Physics.Raycast(commandRay, out commandInfo, Mathf.Infinity) && tNMA != null)
        {
            if (interactedObject.tag == "Interactable Object")
            {
                tNMA.destination = interactedObject.transform.position;
                Debug.Log("Command on an interactable");
                tNMA = null;
            }
            else if (interactedObject.tag == "Hostile")
            {
                Debug.Log("Command to attack Hostile");
                playerMember.GetComponent<CombatIntelligence>().target = interactedObject;
                tNMA.stoppingDistance = playerMember.GetComponent<CombatIntelligence>().attackRange;
                tNMA.destination = interactedObject.transform.position;
                tNMA = null;
                
            }
            else if (interactedObject.tag == "Player")
            {
                Debug.Log("Hit Player");
            }
            else
            {
                tNMA.stoppingDistance = 0.0f;
                tNMA.destination = commandInfo.point;
                tNMA = null;
            }
        }
        playerMember = null;
    }
}

