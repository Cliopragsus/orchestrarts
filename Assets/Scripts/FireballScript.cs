﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballScript : MonoBehaviour
{
    public GameObject fireballChild;
    public Vector3 target;
    public bool allowedToMove;
    private bool exploding;
    public float dmg;
    private AudioSource audioS;
    public AudioClip boom;

    private void Start()
    {
        audioS = GetComponent<AudioSource>();
        //audioS.pi
        allowedToMove = false;
        exploding = false;
        fireballChild = gameObject.transform.Find("Fire_Explosion_01").gameObject;
    }

    private void Update()
    {
        if (allowedToMove)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target, 0.25f);
        }

        if (allowedToMove && Vector3.Distance(gameObject.transform.position, target) < 1 && !exploding)
        {
            gameObject.GetComponent<SphereCollider>().enabled = true;
            exploding = true;
            StartCoroutine(Timer());
            audioS.clip = boom;
            audioS.Play();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Hostile") /*|| other.CompareTag("Player Member")*/)
        {
            other.gameObject.SendMessage("ApplyDamage", dmg);
            Debug.Log("Hit : " + other.gameObject.name);
            //StartCoroutine(Timer());
        }
        else
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), other.GetComponent<SphereCollider>());
            Debug.Log("No viable targets");
            //StartCoroutine(Timer());
        }

    }

    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(0.1f);
        gameObject.GetComponent<SphereCollider>().enabled = false;
        fireballChild.SetActive(true);
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
