﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatIntelligence : MonoBehaviour
{
    UnityEngine.AI.NavMeshAgent nMA;
    Animator anim;

    public static bool isCasting;
    public static bool isStunned;

    public GameObject target;
    public float attackRange;
    public float attackDamage;
    public float maxHealth;
    public float curHealth;
    public float hps; //amount per second

    public bool isMember;
    public bool isFighting;
    public bool isAttacking;
    private bool isTracking;
	
	void Start ()
    {
        nMA = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();
        isFighting = false;
        isAttacking = false;
        isTracking = false;
        isStunned = false;
        isCasting = false;

        StartCoroutine(HealthRegeneration(hps));
	}
	
	
	void Update ()
    {
        if (isFighting != true && target != null)
        {
            Debug.Log("Is Fighting");
            isFighting = true;
            StartCoroutine(Attack());
        }

        if (curHealth <= 0)
        {
            gameObject.SetActive(false);
        }
        
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hostile"))
        {
            Debug.Log("Detected Hostile");
        }

        if (isMember == true && isFighting == false && other.CompareTag("Hostile"))
        {
            nMA.stoppingDistance = 5.0f;
            target = other.gameObject;
            isFighting = true;
            StartCoroutine(Attack());
        }

        if (isMember == false && isFighting == false && other.CompareTag("Player Member"))
        {
            nMA.stoppingDistance = 5.0f;
            target = other.gameObject;
            isFighting = true;
            StartCoroutine(Attack());
        }

        if (other.CompareTag("Projectile"))
        {
            curHealth -= 10;
            Destroy(other.gameObject);
        }
    }

    private IEnumerator Attack()
    {
        isTracking = false;
        gameObject.transform.Find("DetectionCollider").GetComponent<SphereCollider>().enabled = false;
        nMA.stoppingDistance = attackRange;
        float eHealth = target.GetComponent<CombatIntelligence>().curHealth;
        float dist = Vector3.Distance(target.transform.position, gameObject.transform.position);


        if (target != null && dist <= attackRange && eHealth > 0 && !isCasting && !isStunned)
        {
            gameObject.transform.LookAt(target.transform.position);
            isAttacking = true;
            yield return new WaitForSeconds(3);

            isAttacking = false;
            target.SendMessage("ApplyDamage", attackDamage);
            //TextPopUpController.CreateFloatingText(attackDamage.ToString(), transform);
            //Debug.DrawLine(gameObject.transform.Find("FirePos").position, target.transform.Find("FirePos").position, Color.red, 1.0f);

            if (target.GetComponent<CombatIntelligence>().target == null)
            {
                target.GetComponent<CombatIntelligence>().target = this.gameObject;
            }
            StartCoroutine(Attack());
        }
        else if (target != null && dist > attackRange && target != null && eHealth > 0 && !isCasting && !isStunned)
        {
            isTracking = true;
            nMA.destination = target.transform.position;
            yield return new WaitForSeconds(0.1f);
            StartCoroutine(Attack());
        }
        else
        {
            target = null;
            isFighting = false;
            gameObject.transform.Find("DetectionCollider").GetComponent<SphereCollider>().enabled = true;
            anim.Play("Idle");
            yield return null;
        }
    }

    public void ApplyDamage(float dmg)
    {
        curHealth -= dmg;
    }

    private IEnumerator HealthRegeneration(float healthReg)
    {
        yield return new WaitForSeconds(0.5f);
        curHealth += (healthReg / 2);
        StartCoroutine(HealthRegeneration(hps));
    }
}
