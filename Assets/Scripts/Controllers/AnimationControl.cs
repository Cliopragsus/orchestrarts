﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControl : MonoBehaviour
{
    private Animator anim;
    private UnityEngine.AI.NavMeshAgent nMA;

	void Start ()
    {
        nMA = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = gameObject.GetComponent<Animator>();
	}
	
	void Update ()
    {
        if (gameObject.GetComponent<CombatIntelligence>().isAttacking == true)
        {
            anim.Play("Cast");
        }
        else if (nMA.remainingDistance > nMA.stoppingDistance)//&& gameObject.GetComponent<CombatIntelligence>().isFighting != true)
        {
            anim.Play("Walking");
        }
        else
        {
            anim.Play("Idle");
        }
	}
}
