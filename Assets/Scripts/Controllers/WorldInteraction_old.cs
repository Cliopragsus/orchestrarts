﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldInteraction_old : MonoBehaviour
{
    private UnityEngine.AI.NavMeshAgent tNMA; //target's Nav Mesh Agent
    private GameObject playerMember;

    void Start()
    {

    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GetInteraction();
        }

        if (Input.GetMouseButtonUp(0))
        {
            Command();
        }
    }

    void GetInteraction()
    {
        Ray interactionRay = Camera.main.ViewportPointToRay(new Vector3 (0.5f, 0.5f, 0));
        RaycastHit interactionInfo;
        if (Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity))
        {
            GameObject interactedObject = interactionInfo.collider.gameObject;
            if (interactedObject.tag == "Interactable Object")
            {
                //interactedObject.GetComponent<Interactable>();
                Debug.Log("Hit an interactable.");
            }
            else if (interactedObject.tag == "Player Member")
            {
                playerMember = interactedObject.gameObject;
                tNMA = interactedObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
                //agentAgent.destination = interactionInfo.point;
                Debug.Log("Hit a Player Member");
            }
        }
    }

    void Command()
    {
        Ray interactionRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit interactionInfo;
        if (Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity) && tNMA != null)
        {
            GameObject interactedObject = interactionInfo.collider.gameObject;
            if (interactedObject.tag == "Interactable Object")
            {
                tNMA.destination = interactionInfo.point;
                Debug.Log("Command on an interactable");
                tNMA = null;
            }
            else if (interactedObject.tag == "Hostile")
            {
                Debug.Log("Command to attack Hostile");
                playerMember.GetComponent<CombatIntelligence>().target = interactedObject;
                tNMA.stoppingDistance = playerMember.GetComponent<CombatIntelligence>().attackRange;
                tNMA.destination = interactedObject.transform.position;  //interactionInfo.point;
                tNMA = null;
                
            }
            else if (interactedObject.tag == "Player")
            {
                Debug.Log("Hit Player");
            }
            else
            {
                tNMA.stoppingDistance = 0.0f;
                tNMA.destination = interactionInfo.point;
                tNMA = null;
            }
        }
        playerMember = null;
    }
}

