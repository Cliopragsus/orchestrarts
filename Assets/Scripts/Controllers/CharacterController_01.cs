﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController_01 : MonoBehaviour
{
    public float speed = 2.0f;
    public float sensitivity = 2.0f;
    public float gravity;

    CharacterController player;
    public GameObject eyes;

    private float moveFB;
    private float moveLR;

    private float rotX;
    private float rotY;
    private const float YAngleMin = 10.0f;
    private const float YAngleMax = 50.0f;

    void Start ()
    {
        player = GetComponent<CharacterController>();
	}
	
	void Update ()
    {
        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;

        rotX = Input.GetAxis("Mouse X") * sensitivity;
        rotY = Input.GetAxis("Mouse Y") * sensitivity;
        //rotY = Mathf.Clamp(rotY, YAngleMin, YAngleMax);

        Vector3 movement = new Vector3(-moveFB, 0, moveLR);
        transform.Rotate(0, rotX, 0);
        movement = transform.rotation * movement;

        eyes.transform.Rotate(0, 0, -rotY);
        if (Input.GetButtonDown("Jump") && player.isGrounded)
        {
            movement.y = 80;
        }
        else
        {
            movement.y -= gravity;// * Time.deltaTime;
        }

        player.Move(movement * Time.deltaTime);
        
	}
}
