﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextPopUpController : MonoBehaviour
{
    private static TextPopUp popUpTextPrefab;
    private static GameObject canvas;

    public static void InitializeTextController()
    {
        canvas = GameObject.Find("Canvas");
        popUpTextPrefab = Resources.Load<TextPopUp>("Prefabs/PopUpTextParent");
    }

    public static void CreateFloatingText(string text, Transform textLocation)
    {
        TextPopUp instance = Instantiate(popUpTextPrefab);
        instance.transform.SetParent(canvas.transform, false);
        instance.SetText(text);
    }
}
