﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPopUp : MonoBehaviour
{
    public Animator anim;
    private Text damageText;

    private void Start()
    {
        AnimatorClipInfo[] clipInfo = anim.GetCurrentAnimatorClipInfo(0);
        Destroy(gameObject, clipInfo[0].clip.length);

        damageText = anim.GetComponent<Text>();
    }

    public void SetText(string amount)
    {
        damageText.text = amount;
    }
}
