﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController_01 : MonoBehaviour
{
    public bool invertedY;

    public Transform lookAt;
    public Transform camTransform;

    //private Camera cam;

    public float distance = 100.0f;
    public float currentX = 0.0f;
    public float currentY = 0.0f;
    public float sensitivityX = 4.0f;
    public float sensivityY = 1.0f;

    private const float YAngleMin = 10.0f;
    private const float YAngleMax = 50.0f;

    public void Start()
    {
        camTransform = transform;
        //cam = Camera.main;
    }

    private void Update()
    {
        currentX += Input.GetAxis("Mouse X");

        if (invertedY != true)
        {
            currentY -= Input.GetAxis("Mouse Y");
        }
        else
        {
            currentY += Input.GetAxis("Mouse Y");
        }

        currentY = Mathf.Clamp(currentY, YAngleMin, YAngleMax);
    }

    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTransform.position = lookAt.position + rotation * dir;

        camTransform.LookAt(lookAt.position);
    }
}
