﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemberVirtuals : MonoBehaviour
{
    public bool isActivating;

    public virtual void Abilities()
    {
        Debug.Log("Accessing Member-Abilities");
    }
}
