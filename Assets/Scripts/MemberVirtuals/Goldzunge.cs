﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Goldzunge : MemberVirtuals
{
    private GameObject player;
    private NavMeshAgent nMA;

    //private CombatIntelligence CI;
    private WorldInteraction WI;

    public GameObject fireballPrefab;
    public GameObject fireball;
    public GameObject fireballPos;
    public Vector3 fireballTarget;
    public bool fireCharging;

    public bool abilityONEonCooldown;
    public bool abilityTWOonCooldown;

    private void Start()
    {
        fireball = null;
        player = GameObject.Find("Player");
        WI = player.GetComponent<WorldInteraction>();
        nMA = gameObject.GetComponent<NavMeshAgent>();
        //CI = gameObject.GetComponent<CombatIntelligence>();
    }

    private void Update()
    {
        ///////////////////
        // ABILITY ONE
        ///////////////////
        if (Input.GetButton("AbilityONE") && isActivating)
        {
            if (fireCharging == false)
            {
                fireball = Instantiate(fireballPrefab, fireballPos.transform.position, Quaternion.identity);
                fireCharging = true;
            }
            Debug.Log("Charging Fireball!");
        }

        if (Input.GetMouseButtonDown(1) && fireCharging) //causes held abilities to go blind
        {
            isActivating = false;
            Destroy(fireball);
            fireCharging = false;
        }

        if (Input.GetButtonUp("AbilityONE")
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true
            //&& WI.interactedObject.tag == "Hostile"
            && abilityONEonCooldown != true)
        {
            Debug.Log("Ability One FIREBALL Used");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
            //fireBall.SetActive(false);
            //fireCharging = false;
            fireball.GetComponent<FireballScript>().target = WI.interactionPoint;
            abilityONEonCooldown = true;
            StartCoroutine(Fireball(1));
        }
        else if (Input.GetButtonUp("AbilityONE")
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true
            && player.GetComponent<WorldInteraction>().interactedObject.tag != "Hostile")
        {
            Debug.Log("No valid target found for AbilityONE CHARGE");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
        }

        /////////////////
        // Ability TWO
        /////////////////
        if (Input.GetButtonUp("AbilityTWO")
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true
            && abilityTWOonCooldown != true
            && GameObject.Find("Player").GetComponent<WorldInteraction>().interactedObject.tag == "Hostile")
        {
            Debug.Log("Ability TWO SMITE Used");
            abilityTWOonCooldown = true;
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
            //CI.target = player.GetComponent<WorldInteraction>().interactedObject;
            //smiteTarget = WI.interactedObject;
            //Smite();
        }
        else if (Input.GetButtonUp("AbilityTWO") && gameObject.GetComponent<MemberVirtuals>().isActivating == true
         && GameObject.Find("Player").GetComponent<WorldInteraction>().interactedObject.tag != "Hostile")
        {
            Debug.Log("No valid target found for AbilityTWO SMITE");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
        }
    }

    private IEnumerator Fireball(float castTime)
    {
        //Debug.Log("Should be moving any second now?");
        nMA.speed = 0f;
        yield return new WaitForSeconds(castTime);
        fireball.GetComponent<FireballScript>().allowedToMove = true;
        //fireCharging = false;
        nMA.speed = 3.5f;
        StartCoroutine(FireBallCooldown(10f));
    }

    private IEnumerator FireBallCooldown(float cd)
    {
        yield return new WaitForSeconds(cd);
        abilityONEonCooldown = false;
        fireCharging = false;
    }
}
