﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Byson : MemberVirtuals
{
    public GameObject player;
    public bool abilityONEonCooldown;
    public bool abilityTWOonCooldown;

    private void Start()
    {
        player = GameObject.Find("Player");
    }

    public void Update()
    {
        ///////////////////
        // ABILITY ONE
        ///////////////////
        if (Input.GetButtonUp("AbilityONE") 
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true 
            && player.GetComponent<WorldInteraction>().interactedObject.tag == "Hostile" 
            && abilityONEonCooldown != true)
        {
            Debug.Log("Ability One STUN Used");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
            abilityONEonCooldown = true;
            StartCoroutine(Stun());
        }
        else if (Input.GetButtonUp("AbilityONE") 
              && gameObject.GetComponent<MemberVirtuals>().isActivating == true 
              && player.GetComponent<WorldInteraction>().interactedObject.tag != "Hostile")
        {
            Debug.Log("No valid target found for AbilityONE STUN");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
        }

        /////////////////
        // Ability TWO
        /////////////////
        if (Input.GetButtonUp("AbilityTWO") 
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true 
            && abilityTWOonCooldown != true
            && player.GetComponent<WorldInteraction>().interactedObject.tag == "Player Member")
        {
            Debug.Log("Ability Two HEAL Used");
            abilityTWOonCooldown = true;
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
            StartCoroutine(Heal());
        }
        else if (Input.GetButtonUp("AbilityTWO") 
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true 
            && player.GetComponent<WorldInteraction>().interactedObject.tag != "Player Member")
        {
            Debug.Log("No valid target found for AbilityTWO HEAL");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
        }
    }

    public override void Abilities()
    {
        Debug.Log("Accessing Bysons Abilities");
    }

    private IEnumerator Stun()
    {
        GameObject stunnedTarget;
        stunnedTarget = player.GetComponent<WorldInteraction>().interactedObject;
        stunnedTarget.GetComponent<CombatIntelligence>().enabled = false;
        yield return new WaitForSeconds(5);
        stunnedTarget.GetComponent<CombatIntelligence>().enabled = true;
        yield return new WaitForSeconds(5);
        abilityONEonCooldown = false;
    }

    private IEnumerator Heal()
    {
        player.GetComponent<WorldInteraction>().interactedObject.GetComponent<CombatIntelligence>().curHealth += 50;
        yield return new WaitForSeconds(10);
        abilityTWOonCooldown = false;
        Debug.Log("Heal available");
    }
}
