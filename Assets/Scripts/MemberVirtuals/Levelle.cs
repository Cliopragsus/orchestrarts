﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Levelle : MemberVirtuals
{
    private GameObject player;
    private NavMeshAgent nMA;

    private CombatIntelligence CI;
    private WorldInteraction WI;

    public GameObject smitePos;
    public GameObject smiteProjectile;
    public GameObject smiteTarget;

    public bool abilityONEonCooldown;
    public bool abilityTWOonCooldown;

    private AudioSource audioS;
    public AudioClip charge;
    public AudioClip smiteChargeUp;
    public AudioClip smiteDetonate;

    private void Start()
    {
        player = GameObject.Find("Player");
        WI = player.GetComponent<WorldInteraction>();
        nMA = gameObject.GetComponent<NavMeshAgent>();
        CI = gameObject.GetComponent<CombatIntelligence>();
        audioS = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        ///////////////////
        // ABILITY ONE
        ///////////////////
        if (Input.GetButtonUp("AbilityONE") 
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true
            && WI.interactedObject.tag == "Hostile" 
            && abilityONEonCooldown != true)
        {
            Debug.Log("Ability One CHARGE Used");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
            abilityONEonCooldown = true;
            StartCoroutine(Charge());
        }
        else if (Input.GetButtonUp("AbilityONE") 
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true
            && player.GetComponent<WorldInteraction>().interactedObject.tag != "Hostile")
        {
            Debug.Log("No valid target found for AbilityONE CHARGE");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
        }

        /////////////////
        // Ability TWO
        /////////////////
        if (Input.GetButtonUp("AbilityTWO") 
            && gameObject.GetComponent<MemberVirtuals>().isActivating == true 
            && abilityTWOonCooldown != true 
            && GameObject.Find("Player").GetComponent<WorldInteraction>().interactedObject.tag == "Hostile")
        {
            Debug.Log("Ability TWO SMITE Used");
            abilityTWOonCooldown = true;
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
            //CI.target = player.GetComponent<WorldInteraction>().interactedObject;
            smiteTarget = WI.interactedObject;
            Smite();
        }
        else if (Input.GetButtonUp("AbilityTWO") && gameObject.GetComponent<MemberVirtuals>().isActivating == true
         && GameObject.Find("Player").GetComponent<WorldInteraction>().interactedObject.tag != "Hostile")
        {
            Debug.Log("No valid target found for AbilityTWO SMITE");
            gameObject.GetComponent<MemberVirtuals>().isActivating = false;
        }
    }

    private IEnumerator Charge()
    {
        StartCoroutine(ChargeCooldown(10));
        audioS.clip = charge;
        audioS.Play();
        //yield return new WaitForSeconds(0.5f);
        CI.target = WI.interactedObject;
        nMA.acceleration += 10.0f;
        nMA.speed += 50.0f;
        yield return new WaitForSeconds(1.5f);
        //gameObject.GetComponent<CombatIntelligence>().target = player.GetComponent<WorldInteraction>().interactedObject;
        nMA.acceleration -= 10.0f;
        nMA.speed -= 50.0f;
    }

    private void Smite()
    {
        //CI.target = player.GetComponent<WorldInteraction>().interactedObject;
        StartCoroutine(CastTime(1));
        StartCoroutine(SmiteCooldown(10));
        StartCoroutine(SmiteSound(smiteChargeUp, false, 1f));
    }

    private IEnumerator CastTime(int castTime)
    {
        nMA.speed = 0;
        smiteProjectile.SetActive(true);
        CombatIntelligence.isCasting = true;

        yield return new WaitForSeconds(castTime);

        StartCoroutine(SmiteSound(smiteDetonate, true,  1));
        CombatIntelligence.isCasting = false;
        nMA.speed = 3.5f;
        smiteProjectile.transform.position = smiteTarget.transform.position;
        yield return new WaitForSeconds(0.1f);
        smiteTarget.GetComponent<CombatIntelligence>().curHealth -= 10f;
        if (smiteTarget.GetComponent<CombatIntelligence>().isFighting != true)
        {
            smiteTarget.GetComponent<CombatIntelligence>().target = gameObject;
        }
        yield return new WaitForSeconds(0.1f);
        smiteProjectile.SetActive(false);
        smiteProjectile.transform.position = smitePos.transform.position;
    }

    private IEnumerator ChargeCooldown(float cd)
    {
        yield return new WaitForSeconds(cd);
        abilityONEonCooldown = false;
        Debug.Log("Charge avaiable");
    }

    private IEnumerator SmiteCooldown(float cd)
    {
        CI.hps += 10f;
        yield return new WaitForSeconds(cd);
        CI.hps -= 10f;
        abilityTWOonCooldown = false;
        Debug.Log("Smite avaiable");
    }

    private IEnumerator SmiteSound(AudioClip sound, bool shouldBeStopped, float timer)
    {
        audioS.clip = sound;
        audioS.Play();
        if (shouldBeStopped)
        {
            yield return new WaitForSeconds(timer);
            audioS.Stop();
        }
    }
}
